import React, { useState } from 'react';

const AccordionItem = (props) => {
  const [accordionActive, setAccordionActive] = useState(false);

  return (
    <div className="accordion__item">
      <div
        className="accordion__head"
        onClick={() => {
          setAccordionActive(!accordionActive);
        }}
      >
        {props.name}
        <div className={accordionActive ? 'active' : null}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </div>
      </div>
      {accordionActive && (
        <div className="accordion__body">
          <p>Username: {props.username}</p>
          <p>Email: {props.email}</p>
          <p>Phone: {props.phone}</p>
          <p>Website: {props.website}</p>
        </div>
      )}
    </div>
  );
};

export default AccordionItem;
