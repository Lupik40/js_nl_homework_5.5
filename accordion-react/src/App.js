import React, { useState, useEffect } from 'react';
import './App.scss';
import AccordionItem from './components/AccordionItem';

const App = () => {
  const [accordionItems, setAccordionItems] = useState([]);

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then((response) => response.json())
      .then((json) => setAccordionItems(json));
  }, []);

  return (
    <div className="accordion">
      {accordionItems.map((accordion) => {
        return (
          <AccordionItem
            name={accordion.name}
            username={accordion.username}
            email={accordion.email}
            phone={accordion.phone}
            website={accordion.website}
            key={accordion.id}
          />
        );
      })}
    </div>
  );
};

export default App;
